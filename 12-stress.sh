#!/bin/bash
eval $(cat vm.config)

ip=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

service1="hello-ruby"
service2="hello-js"
namespace="demo"

url1="http://${service1}.${namespace}.${ip}.xip.io"
url2="http://${service2}.${namespace}.${ip}.xip.io"

#hey -z 60s -c 100 ${url}

hey -z 20s -c 30 -m POST -T "application/json" -d '{"name":"john doe"}' ${url1}&
hey -z 20s -c 30 -m POST -T "application/json" -d '{"name":"jane doe"}' ${url2}&

# TODO voir pour rendre le test plus parlant en faisant un premier stress tests sans autoscalling puis un autre avec
# et pas forcement un stress test sur les 2
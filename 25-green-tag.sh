#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="tada"
namespace="demo"

kn service update ${service} \
  --namespace ${namespace} \
  --tag ${service}-green=green

kn revision list -s ${service} -n ${namespace}
echo ""
kn route describe ${service} -n ${namespace}





#!/bin/bash
. ./env.sh

kubectl port-forward --namespace knative-monitoring \
$(kubectl get pods --namespace knative-monitoring \
--selector=app=prometheus \
--output=jsonpath="{.items[0].metadata.name}") \
9090
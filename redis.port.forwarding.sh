#!/bin/bash
# Set environment variables
. ./env.sh

NAMESPACE="database"

kubectl port-forward --namespace ${NAMESPACE} svc/redis-mother 6379:6379


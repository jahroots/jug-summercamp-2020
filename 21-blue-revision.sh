#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="tada"
namespace="demo"

read -d '' CODE << EOF
function main(params) {
  return {
    message: "🎉 tada! Hello " + params.getString("name"),
    revision: "🔵"
  }
}
EOF

# 👋 create a revision and update the traffic

kn service update ${service} \
--namespace ${namespace} \
--env FUNCTION_CODE="$CODE" \
--revision-name blue \
--traffic ${service}-blue=100

kn revision list -s ${service} -n ${namespace}
echo ""
kn route describe ${service} -n ${namespace}



#!/bin/bash
# Set environment variables
. ./env.sh

NAMESPACE="database"
kubectl create namespace ${NAMESPACE}
# PersistentVolumeClaim
kubectl apply -f redis.pvc.yaml -n ${NAMESPACE}
# Install Redis
kubectl apply -f redis.yaml -n ${NAMESPACE}


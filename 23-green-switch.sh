#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="tada"
namespace="demo"

kn service update ${service} \
--namespace ${namespace} \
--traffic ${service}-blue=0 \
--traffic ${service}-green=100

kn revision list -s ${service} -n ${namespace}

#kn route describe ${service} -n ${namespace}




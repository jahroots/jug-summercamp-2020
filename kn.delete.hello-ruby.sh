#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="hello-ruby"
namespace="demo"

kn service delete ${service} -n ${namespace}

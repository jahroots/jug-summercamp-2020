#!/bin/bash
# Set environment variables
# use the local kubectl and kn CLI 
. ./env.sh

service="hey"
namespace="demo"

kn service update ${service} \
  --namespace ${namespace} \
  --traffic ${service}-yellow="50" \
  --traffic ${service}-orange="50"

kn revision list -s ${service} -n ${namespace}

